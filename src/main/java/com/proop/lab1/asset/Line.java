package com.proop.lab1.asset;

public class Line {

    Integer length;

    public Line(Integer length) {
        this.length = length;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    @Override
    public String toString() {
        return String.format("[%d]", length);
    }

}
