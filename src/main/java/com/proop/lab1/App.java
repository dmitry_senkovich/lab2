package com.proop.lab1;

import com.proop.lab1.frontend.AppForm;

import javax.swing.*;

public class App {

    public static void main(String... args) {
        JFrame shapeFrame = new JFrame("Lab 2");
        AppForm appForm = new AppForm();
        shapeFrame.add(appForm);
        shapeFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        shapeFrame.pack();
        shapeFrame.setVisible(true);
    }

}
