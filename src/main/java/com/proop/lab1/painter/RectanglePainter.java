package com.proop.lab1.painter;

public class RectanglePainter implements ShapePainter {

    @Override
    public String paint(Integer[] coordinates) {
        return String.format("Rectangle((%d, %d), %d, %d)",
                coordinates[0], coordinates[1], coordinates[2], coordinates[3]);
    }

}
