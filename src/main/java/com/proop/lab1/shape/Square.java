package com.proop.lab1.shape;

import com.proop.lab1.asset.Line;
import com.proop.lab1.asset.Point;

public class Square implements Shape {

    public static final String SHAPE_NAME = "square";

    private Point topLeftVertex;
    private Line side;

    public Square(Point topLeftVertex, Line side) {
        this.topLeftVertex = topLeftVertex;
        this.side = side;
    }

    @Override
    public Integer[] getCoordinates() {
        return new Integer[]{topLeftVertex.getX(), topLeftVertex.getY(), side.getLength()};
    }

    @Override
    public String getShapeName() {
        return SHAPE_NAME;
    }

}
