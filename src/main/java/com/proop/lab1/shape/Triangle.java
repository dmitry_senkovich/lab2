package com.proop.lab1.shape;

import com.proop.lab1.asset.Point;

public class Triangle implements Shape {

    public static final String SHAPE_NAME = "triangle";

    private Point firstVertex;
    private Point secondVertex;
    private Point thirdVertex;

    public Triangle(Point firstVertex, Point secondVertex, Point thirdVertex) {
        this.firstVertex = firstVertex;
        this.secondVertex = secondVertex;
        this.thirdVertex = thirdVertex;
    }

    @Override
    public Integer[] getCoordinates() {
        return new Integer[]{firstVertex.getX(), firstVertex.getY(), secondVertex.getX(), secondVertex.getY(), thirdVertex.getX(), thirdVertex.getY()};
    }

    @Override
    public String getShapeName() {
        return SHAPE_NAME;
    }
}
