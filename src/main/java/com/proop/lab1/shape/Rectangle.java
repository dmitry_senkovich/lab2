package com.proop.lab1.shape;

import com.proop.lab1.asset.Line;
import com.proop.lab1.asset.Point;

public class Rectangle implements Shape {

    public static final String SHAPE_NAME = "rectangle";

    private Point topLeftVertex;
    private Line verticalSide;
    private Line horizontalSide;

    public Rectangle(Point topLeftVertex, Line verticalSide, Line horizontalSide) {
        this.topLeftVertex = topLeftVertex;
        this.verticalSide = verticalSide;
        this.horizontalSide = horizontalSide;
    }

    @Override
    public Integer[] getCoordinates() {
        return new Integer[]{topLeftVertex.getX(), topLeftVertex.getY(), verticalSide.getLength(), horizontalSide.getLength()};
    }

    @Override
    public String getShapeName() {
        return SHAPE_NAME;
    }
}
