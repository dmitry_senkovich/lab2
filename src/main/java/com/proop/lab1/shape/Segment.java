package com.proop.lab1.shape;

import com.proop.lab1.asset.Point;

public class Segment implements Shape {

    public static final String SHAPE_NAME = "segment";

    private Point start;
    private Point end;

    public Segment(Point start, Point end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public Integer[] getCoordinates() {
        return new Integer[]{start.getX(), start.getY(), end.getX(), end.getY()};
    }

    @Override
    public String getShapeName() {
        return SHAPE_NAME;
    }

}
