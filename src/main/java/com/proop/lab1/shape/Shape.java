package com.proop.lab1.shape;

public interface Shape {

    Integer[] getCoordinates();

    String getShapeName();

}
