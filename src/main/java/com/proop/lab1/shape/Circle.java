package com.proop.lab1.shape;

import com.proop.lab1.asset.Line;
import com.proop.lab1.asset.Point;

public class Circle implements Shape {

    public static final String SHAPE_NAME = "circle";

    private Point center;
    private Line radius;

    public Circle(Point center, Line radius) {
        this.center = center;
        this.radius = radius;
    }


    @Override
    public Integer[] getCoordinates() {
        return new Integer[] {center.getX(), center.getY(), radius.getLength()};
    }

    @Override
    public String getShapeName() {
        return SHAPE_NAME;
    }
}
