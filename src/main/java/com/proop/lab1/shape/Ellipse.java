package com.proop.lab1.shape;

import com.proop.lab1.asset.Line;
import com.proop.lab1.asset.Point;

public class Ellipse implements Shape {

    public static final String SHAPE_NAME = "ellipse";

    private Point center;
    private Line semiMinorAxis;
    private Line semiMajorAxis;

    public Ellipse(Point center, Line semiMinorAxis, Line semiMajorAxis) {
        this.center = center;
        this.semiMinorAxis = semiMinorAxis;
        this.semiMajorAxis = semiMajorAxis;
    }

    @Override
    public Integer[] getCoordinates() {
        return new Integer[]{center.getX(), center.getY(), semiMinorAxis.getLength(), semiMajorAxis.getLength()};
    }

    @Override
    public String getShapeName() {
        return SHAPE_NAME;
    }

}
