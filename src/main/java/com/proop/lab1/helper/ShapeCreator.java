package com.proop.lab1.helper;

import com.proop.lab1.asset.Line;
import com.proop.lab1.asset.Point;
import com.proop.lab1.shape.Shape;
import com.proop.lab1.shape.factory.ShapeFactory;
import com.proop.lab1.shape.factory.abstractfactory.ShapeFactoryAbstractFactory;

import java.util.List;

public class ShapeCreator {

    private static final ShapeCreator SHAPE_CREATOR = new ShapeCreator();

    private ShapeRegistry shapeRegistry = ShapeRegistry.getInstance();
    private ShapeFactoryAbstractFactory shapeFactoryAbstractFactory = ShapeFactoryAbstractFactory.getInstance();

    private ShapeCreator() {}

    public Shape createShape(String shapeName, List<Point> points, List<Line> lines) {
        Class<? extends Shape> shapeClass = shapeRegistry.getRegisteredShapes().get(shapeName);
        ShapeFactory shapeFactory = shapeFactoryAbstractFactory.getFactoryObject(shapeClass);

        return shapeFactory.createShape(points, lines);
    }

    public static ShapeCreator getInstance() {
        return SHAPE_CREATOR;
    }

}
