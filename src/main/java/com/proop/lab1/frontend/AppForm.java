/*
 * Created by JFormDesigner on Sun Mar 11 01:41:40 MSK 2018
 */

package com.proop.lab1.frontend;

import com.proop.lab1.asset.Line;
import com.proop.lab1.asset.Point;
import com.proop.lab1.backend.AppBackend;

import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.*;

public class AppForm extends JPanel {

    private AppBackend appBackend = AppBackend.getInstance();

    public AppForm() {
        initComponents();
        appBackend.setCurrentShapeName(appBackend.getShapeNames().get(0));
    }

    private void shapeComboBoxItemStateChanged(ItemEvent e) {
        appBackend.setCurrentShapeName(shapeComboBox.getSelectedItem().toString());
    }

    private void addPointButtonMousePressed(MouseEvent e) {
        try {
            Integer x = Integer.valueOf(xTextField.getText());
            Integer y = Integer.valueOf(yTextField.getText());
            Point point = new Point(x, y);
            appBackend.addPoint(point);
            xTextField.setText("");
            yTextField.setText("");
            currentPointsTextArea.setText(appBackend.getCurrentPoints());
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(this, "Failed to add a new point. Make sure x and y are valid",
                    "Error while adding a new point",  JOptionPane.ERROR_MESSAGE);
            System.out.println("Failed to add a new point");
        }
    }

    private void addLineButtonMousePressed(MouseEvent e) {
        try {
            Integer length = Integer.valueOf(lengthTextField.getText());
            Line line = new Line(length);
            appBackend.addLine(line);
            lengthTextField.setText("");
            currentLinesTextArea.setText(appBackend.getCurrentLines());
        } catch (Exception exception) {
            JOptionPane.showMessageDialog(this, "Failed to add a new line. Make sure length is valid",
                    "Error while adding a new line",  JOptionPane.ERROR_MESSAGE);
            System.out.println("Failed to add a new line");
        }
    }

    private void resetCurrentShapeButtonMousePressed(MouseEvent e) {
        resetCurrentShape();
    }

    private void resetCurrentShape() {
        appBackend.resetCurrentShape();
        appBackend.setCurrentShapeName(appBackend.getShapeNames().get(0));
        xTextField.setText("");
        yTextField.setText("");
        lengthTextField.setText("");
        currentPointsTextArea.setText("");
        currentLinesTextArea.setText("");
    }

    private void addShapeButtonMousePressed(MouseEvent e) {
        boolean created = appBackend.createShape();
        if (created) {
            resetCurrentShape();
            JOptionPane.showMessageDialog(this, "Shape is created!");
        } else {
            JOptionPane.showMessageDialog(this,
                    "Failed to create a new shape. Please, recheck current coordinates",
                    "Error while creating a shape",  JOptionPane.ERROR_MESSAGE);
        }
    }

    private void resetShapesButtonMousePressed(MouseEvent e) {
        resetCurrentShape();
        appBackend.resetCurrentShapes();
        shapesTextArea.setText("");
    }

    private void showShapesButtonMousePressed(MouseEvent e) {
        shapesTextArea.setText(appBackend.drawShapes());
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Dmitry Senkovich
        xTextField = new JTextField();
        yTextField = new JTextField();
        shapeComboBox = new JComboBox(appBackend.getShapeNames().toArray());
        addPointButton = new JButton();
        lengthTextField = new JTextField();
        addLineButton = new JButton();
        addShapeButton = new JButton();
        shapesScrollPane = new JScrollPane();
        shapesTextArea = new JTextArea();
        showShapesButton = new JButton();
        currentPointsTextArea = new JTextArea();
        currentLinesTextArea = new JTextArea();
        resetCurrentShapeButton = new JButton();
        resetShapesButton = new JButton();

        //======== this ========

        // JFormDesigner evaluation mark
        setBorder(new javax.swing.border.CompoundBorder(
            new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
                javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

        setLayout(null);
        add(xTextField);
        xTextField.setBounds(10, 55, 75, 30);
        add(yTextField);
        yTextField.setBounds(100, 55, 75, 30);

        //---- shapeComboBox ----
        shapeComboBox.addItemListener(e -> shapeComboBoxItemStateChanged(e));
        add(shapeComboBox);
        shapeComboBox.setBounds(10, 20, 75, shapeComboBox.getPreferredSize().height);

        //---- addPointButton ----
        addPointButton.setText("Add Point");
        addPointButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                addPointButtonMousePressed(e);
            }
        });
        add(addPointButton);
        addPointButton.setBounds(205, 55, 105, 30);
        add(lengthTextField);
        lengthTextField.setBounds(10, 205, 75, 30);

        //---- addLineButton ----
        addLineButton.setText("Add Line");
        addLineButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                addLineButtonMousePressed(e);
            }
        });
        add(addLineButton);
        addLineButton.setBounds(205, 205, 105, 30);

        //---- addShapeButton ----
        addShapeButton.setText("Add Shape");
        addShapeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                addShapeButtonMousePressed(e);
            }
        });
        add(addShapeButton);
        addShapeButton.setBounds(205, 340, 105, 30);

        //======== shapesScrollPane ========
        {

            //---- shapesTextArea ----
            shapesTextArea.setEnabled(false);
            shapesTextArea.setEditable(false);
            shapesScrollPane.setViewportView(shapesTextArea);
        }
        add(shapesScrollPane);
        shapesScrollPane.setBounds(15, 385, 370, 80);

        //---- showShapesButton ----
        showShapesButton.setText("Show Shapes");
        showShapesButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                showShapesButtonMousePressed(e);
            }
        });
        add(showShapesButton);
        showShapesButton.setBounds(205, 480, 105, 30);

        //---- currentPointsTextArea ----
        currentPointsTextArea.setEnabled(false);
        currentPointsTextArea.setEditable(false);
        add(currentPointsTextArea);
        currentPointsTextArea.setBounds(10, 105, 373, 78);

        //---- currentLinesTextArea ----
        currentLinesTextArea.setEnabled(false);
        currentLinesTextArea.setEditable(false);
        add(currentLinesTextArea);
        currentLinesTextArea.setBounds(10, 250, 373, 78);

        //---- resetCurrentShapeButton ----
        resetCurrentShapeButton.setText("Reset");
        resetCurrentShapeButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                resetCurrentShapeButtonMousePressed(e);
            }
        });
        add(resetCurrentShapeButton);
        resetCurrentShapeButton.setBounds(55, 340, 105, 30);

        //---- resetShapesButton ----
        resetShapesButton.setText("Reset");
        resetShapesButton.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                resetShapesButtonMousePressed(e);
            }
        });
        add(resetShapesButton);
        resetShapesButton.setBounds(55, 480, 105, 30);

        setPreferredSize(new Dimension(400, 525));
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Dmitry Senkovich
    private JTextField xTextField;
    private JTextField yTextField;
    private JComboBox shapeComboBox;
    private JButton addPointButton;
    private JTextField lengthTextField;
    private JButton addLineButton;
    private JButton addShapeButton;
    private JScrollPane shapesScrollPane;
    private JTextArea shapesTextArea;
    private JButton showShapesButton;
    private JTextArea currentPointsTextArea;
    private JTextArea currentLinesTextArea;
    private JButton resetCurrentShapeButton;
    private JButton resetShapesButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

}
